/*
	Scenario:
		We have a REST API for our Users resource.
		We have an array of User IDs of followers (think twitter)
		Let fetch the profiles as part of a search module
		we're creating.

		Timeout is set below and fetch() should be rejected if
		latency time is longer than the timeout.
*/
const followers = [2,13,1,9];
const timeout = 3000;
const start = new Date().getTime();
console.log('script init', start);

let latency = Math.ceil(Math.random() * 10000);
let end;
let elapsed;
console.log('latency', latency);

/*
	Timeout Helper for fetch() calls so that
	we don't have to wait too long
	returns Promise
*/
const fetchTimeout = () =>
	new Promise ((resolve, reject) => {
		setTimeout(() => {
			reject(new Error(`timeout after ${timeout} ms`))
		}, timeout)
	});

/*
	General Fetch Function
	@params:
		url
	returns Promise
*/

const fetch = (url) =>
	new Promise ((resolve, reject) => {
		if ( typeof(url) !== 'string' )
			reject(new Error('invalid url'));
		fetchTimeout().catch((err) => reject(err));
		let user = {
			id: 1,
			name: 'Jess Petrova',
			age: '24',
			email: 'jess.p@example.com',
			bio: 'lorem ipsum set amet dolor.'
		};
		setTimeout(()=>{
			resolve(user);
		}, latency)
	});

/*
	Config
*/

const API = {};
API.users = 'http://example.com/users';

/*
	Users
*/

let users = {};

users.get = (id) => {
	return fetch(API.users+'/'+id)
		.then((user) => {
			return user;
		})
		.catch((err) => {
			return err;
		});
}

users.get(12)
	.then((data) => {
		console.log(data);
		end = new Date().getTime();
		elapsed = end - start;
		console.log('script ended after', elapsed);
	})
	.catch((err) => {
		console.log(err);
		end = new Date().getTime();
		elapsed = end - start;
		console.log('script ended after', elapsed);
	});
